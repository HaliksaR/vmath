package method.hord

import kotlin.math.pow
import kotlin.math.abs

private var EPS = 0.00001

class Hord {
    class Points(Left: Double, Right: Double) {
        var left: Double = Left
        var right: Double = Right
        var middle: Double = Double.NaN
    }

    data class InitalValues(
        var powX: Int,
        var variable: Double,
        var pointOne: Double,
        var pointTWo: Double
    )

    companion object {
        fun methodHord(): Int {
            val values = InitalValues(2, 3.0, 1.0, 2.0)
            var points = Points(values.pointOne, values.pointTWo)
            println("Print condition: ")
            points.middle = middleFunc(points, values)
            var ysl = points.middle
            var steps = 1
            var temp: Double
            while (abs(ysl) > EPS) {
                temp = points.middle
                println("Steps: $steps, Middle: ${"%.1f".format(points.middle)}")
                middleOfInterval(points, values)
                points.middle = middleFunc(points, values)
                ysl = points.middle - temp
                println("[${"%.1f".format(points.left)};${"%.1f".format(points.right)}]")
                steps++
            }
            println("Answer: ${"%.1f\n".format(points.middle)}")
            return 0
        }

        private fun powExtremePoints(num: Double, data: InitalValues) = num.pow(data.powX) - data.variable

        private fun middleFunc(points: Hord.Points, data: InitalValues) = (
                (points.left * powExtremePoints(points.right, data)) -
                        (points.right * powExtremePoints(points.left, data))) /
                (powExtremePoints(points.right, data) - powExtremePoints(points.left, data))

        private fun middleOfInterval(points: Points, data: InitalValues) {
            if (powExtremePoints(points.left, data) * powExtremePoints(points.middle, data) > 0)
                points.left = points.middle
            else
                points.right = points.middle
        }
    }
}
