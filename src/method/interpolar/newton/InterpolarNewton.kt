package method.interpolar.newton

import FileFun.fileReaderInterpolar
import kotlin.math.pow

class InterpolarNewton {
    companion object {
        fun methodInterpolarNewton(): Int {
            val array: Array<DoubleArray> = fileReaderInterpolar("Files/Interpolar/Interpolar.txt") ?: return -1
            print("Search y for x: ")
            val x0 = readLine()!!.toDouble()
            print("Step: ")
            val step = readLine()!!.toDouble()
            for (i in array.indices) {
                println(array[i][0].toString() + " " + array[i][1].toString())
            }
            println(interpolarNewton(x0, array, step))
            return 0
        }

        private fun interpolarNewton(x0: Double, array: Array<DoubleArray>, step: Double): Double {
            val tempArray = Array(array.size + 2) {
                DoubleArray(array.size + 1) { 0.0 }
            }
            for (i in 0..1) {
                for (j in array.indices + 1) {
                    if (i == 0) tempArray[i][j] = array[j][0]
                    else if (i == 1) tempArray[i][j] = array[j][1]
                }
            }
            var temp = array.size - 1
            for (i in 2 until (array.size + 1)) {
                for (j in 0..temp)
                    tempArray[i][j] = tempArray[i - 1][j + 1] - tempArray[i - 1][j]
                temp--
            }
            val dy = DoubleArray(array.size + 1)
            for (i in array.indices + 1)
                dy[i] = tempArray[i + 1][0]
            var result = dy[0]
            val xn = DoubleArray(array.size)
            xn[0] = x0 - tempArray[0][0]

            for (i in 1 until array.size - 1) {
                val temp2 = xn[i - 1] * (x0 - tempArray[0][i])
                xn[i] = temp2
            }
            temp = array.size
            var factor = 1
            for (i in 1..temp) {
                factor *= i
                result += (dy[i] * xn[i - 1]) / (factor * step.pow(i))
            }
            return result
        }
    }
}

//https://sohabr.net/habr/post/252825/