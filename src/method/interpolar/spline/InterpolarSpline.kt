package method.interpolar.spline

import FileFun.fileReaderInterpolar
import PrintResurses.printMatrix

class InterpolarSpline {
    companion object {
        fun methodInterpolarSpline(): Int {
            val array: Array<DoubleArray> = fileReaderInterpolar("Files/Interpolar/Interpolar.txt") ?: return -1
            print("Search y for x: ")
            val x0 = readLine()!!.toDouble()
            for (i in array.indices)
                println(array[i][0].toString() + " " + array[i][1].toString())
            println(buildSpline(array, x0))
            return 0
        }

        private fun buildSpline(array: Array<DoubleArray>, x0: Double): Double {
            var top_i = 0
            var vectorM = DoubleArray(array.size)
            var h = DoubleArray(array.size)
            val matrixC = Array(array.size - 2) {
                DoubleArray(array.size - 1)
            }
            for (i in array.indices)
                if (x0 > array[i][0]) top_i++
            h = funcH(h, array)
            vectorM = funcVectorM(matrixC, vectorM, h, array)
            return (vectorM[top_i - 1] * (array[top_i][0] - x0) * (array[top_i][0] - x0) * (array[top_i][0] - x0) / (6 * h[top_i])
                    + vectorM[top_i] * (x0 - array[top_i - 1][0]) * (x0 - array[top_i - 1][0]) * (x0 - array[top_i - 1][0]) / (6 * h[top_i])
                    + (array[top_i - 1][1] - vectorM[top_i - 1] * h[top_i] * h[top_i] / 6) * ((array[top_i][0] - x0) / h[top_i])
                    + (array[top_i][1] - vectorM[top_i] * h[top_i] * h[top_i] / 6) * ((x0 - array[top_i - 1][0]) / h[top_i]))

        }

        private fun funcVectorM(
            matrixC: Array<DoubleArray>,
            vectorM: DoubleArray,
            h: DoubleArray,
            array: Array<DoubleArray>
        ): DoubleArray {
            val rows = array.size - 2
            val col = array.size - 1

            val d = DoubleArray(rows)
            var result = DoubleArray(rows)
            for (i in array.indices)
                vectorM[i] = 0.0
            for (i in 0 until rows)
                d[i] = (array[i + 2][1] - array[i + 1][1]) / h[i + 2] - (array[i + 1][1] - array[i][1]) / (h[i + 1])
            for (i in 0 until rows) {
                for (j in 0 until col) {
                    if (i == j)
                        matrixC[i][j] = (h[i + 1] + h[i + 2]) / 3
                    if (j > i)
                        matrixC[i][j] = h[i + 2] / 6
                    if (i > j)
                        matrixC[i][j] = h[i + 1] / 6
                    if (j == col - 1)
                        matrixC[i][j] = d[i]
                }
            }
            printMatrix(matrixC)
            result = classic_gaus(result, matrixC, rows, col)
            for (i in result.indices)
                println(result[i])
            for (i in array.indices)
                vectorM[i] = 0.0
            vectorM[1] = result[0]
            vectorM[2] = result[1]
            return vectorM
        }

        private fun classic_gaus(result: DoubleArray, matrixC: Array<DoubleArray>, rows: Int, col: Int): DoubleArray {
            var d: Double
            var main_line: DoubleArray
            var operator_line: DoubleArray

            for (i in 0 until rows - 1) {
                main_line = matrixC[i]
                for (j in i + 1 until rows - 1) {
                    operator_line = matrixC[i]
                    d = operator_line[i] / main_line[i]
                    for (k in i until col)
                        matrixC[j][k] = operator_line[k] - main_line[k] * d
                }
            }
            for (i in rows downTo 1) {
                operator_line = matrixC[i - 1]
                for (j in i until col - 1)
                    operator_line[col - 1] -= result[j] * operator_line[j]
                result[i - 1] = operator_line[col - 1] / operator_line[i - 1]
            }
            return result
        }

        private fun funcH(h: DoubleArray, array: Array<DoubleArray>): DoubleArray {
            for (i in array.size - 1 downTo 1)
                h[i] = array[i][0] - array[i - 1][0]
            return h
        }
    }
}

