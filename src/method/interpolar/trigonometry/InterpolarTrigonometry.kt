package method.interpolar.trigonometry

import FileFun.fileReaderline
import com.panayotis.gnuplot.JavaPlot
import com.panayotis.gnuplot.plot.DataSetPlot
import com.panayotis.gnuplot.style.PlotStyle
import com.panayotis.gnuplot.style.Style
import java.lang.Math.*

class InterpolarTrigonometry {

    class Point(val x: Double, val y: Double) {
        fun printPoint() {
            println("(${"%.3f".format(x)}, ${"%.3f".format(y)})")
        }

        override fun toString(): String {
            return "(${"%.3f".format(x)}, ${"%.3f".format(y)})"
        }
    }

    class Points {
        var points: Array<Point?>
            internal set

        val pointsNumber: Int
            get() = points.size

        constructor(pointsNumber: Int) {
            points = arrayOfNulls(pointsNumber)
        }

        constructor(filePath: String) {
            val points = PointsIO.readPointsFromFile(filePath)
            this.points = points
        }

        val pointsInArray: Array<DoubleArray?>
            get() {
                if (points.isEmpty())
                    return emptyArray()
                val pointsArray = arrayOfNulls<DoubleArray>(points.size)
                for (i in points.indices)
                    pointsArray[i] = DoubleArray(2)
                var index = 0
                points.forEach { point ->
                    if (point != null) {
                        pointsArray[index]!![0] = point.x
                    }
                    if (point != null) {
                        pointsArray[index]!![1] = point.y
                    }
                    index++
                }
                return pointsArray
            }

        fun setPoint(index: Int, point: Point) {
            if (points.size <= index)
                points = arrayOfNulls(index + 1)
            points[index] = point
        }

        fun printPoints() {
            System.out.printf("Number of points: %d\n", points.size)
            for (i in points.indices) {
                System.out.printf("Point number %3d: ", i + 1)
                if (points[i] != null) points[i]!!.printPoint()
                println()
            }
            println()
        }
    }

    object PointsIO {
        fun readPointsFromFile(filePath: String): Array<Point?> {
            val pointDoubleArray = fileReaderline(filePath)
            pointsNumber = pointDoubleArray.size
            val points = Array<Point?>(pointsNumber) { null }
            var x: Double
            var y: Double
            for (i in pointDoubleArray.indices) {
                x = i.toDouble() * 2.0 * PI / pointsNumber
                y = pointDoubleArray[i]
                points[i] = Point(x, y)
            }
            println("----------------------------------")
            for (i in points.indices)
                println(points[i])
            println("----------------------------------")
            return points
        }
    }

    companion object {
        private lateinit var points: Points
        private var pointsNumber: Int = 0
        private var degree: Int = 0
        private var a: DoubleArray? = null
        private var b: DoubleArray? = null
        private var valuesPoints: Points? = null

        fun methodInterpolarTrigonometry(): Int {
            points = Points("Files/trig/Trig1.txt")
            pointsNumber = points.pointsNumber
            setPolynomialDegree()
            calculateFactors()
            calculateValuesPoints(10000)
            visualize()
            return 0
        }

        private fun calculateValuesPoints(valuesNumber: Int) {
            valuesPoints = Points(valuesNumber)
            val begin = points.points[0]!!.x
            val end = points.points[points.pointsNumber - 1]!!.x
            var x: Double
            var y: Double
            val jump = (end - begin) / (valuesNumber - 1)
            for (i in 0 until valuesNumber) {
                x = begin + i * jump
                y = calculateValue(x)
                valuesPoints!!.setPoint(i, Point(x, y))
                println("x = ${"%.3f".format(x)}, y = ${"%.3f".format(y)}")
            }
        }

        private fun printFactors() {
            for (i in a!!.indices)
                println("a[" + i + "]=" + a!![i] + ", b[" + i + "]=" + b!![i])
        }

        private fun calculateFactors() {
            a = DoubleArray(degree + 1)
            b = DoubleArray(degree + 1)
            var cosinusSum = 0.0
            var sinusSum = 0.0
            var x: Double
            var y: Double
            for (j in 0..degree) {
                for (k in 0 until pointsNumber) {
                    y = points.points[k]!!.y
                    x = points.points[k]!!.x
                    cosinusSum += y * cos(j * x)
                    sinusSum += y * sin(j * x)
                }
                a!![j] = 2.0 / pointsNumber * cosinusSum //4.19b
                b!![j] = 2.0 / pointsNumber * sinusSum //4.19b

                cosinusSum = 0.0
                sinusSum = 0.0
            }
        }

        private fun setPolynomialDegree() {
            degree = (when {
                pointsNumber == 0 -> 0
                pointsNumber % 2 == 0 -> pointsNumber / 2
                else -> (pointsNumber - 1) / 2
            })
        }

        private fun calculateValue(x: Double): Double {
            var value: Double
            var sum = 0.0
            if (degree % 2 == 0) {
                value = a!![0] / 2
                for (k in 1 until degree)
                    sum += a!![k] * cos(k * x) + b!![k] * sin(k * x)
                value += a!![degree] / 2 * cos(degree * x)
            } else {
                value = a!![0] / 2
                for (k in 1..degree)
                    sum += a!![k] * cos(k * x) + b!![k] * sin(k * x)
            }
            return value + sum
        }

        private fun visualize() {
            val p = JavaPlot()
            p.setTitle("Trigonometric interpolation")
            // measurement points
            val pointsSet: DataSetPlot?
            pointsSet = DataSetPlot(points.pointsInArray)
            pointsSet.setTitle("Measurement points")
            // interpolation function
            var interpolationSet: DataSetPlot? = null
            if (valuesPoints != null)
                interpolationSet = DataSetPlot(valuesPoints!!.pointsInArray)
            interpolationSet?.setTitle("Interpolation")
            // plotting measurement points
            var style = PlotStyle(Style.POINTS)
            style.setPointType(7)
            style.setPointSize(2)
            pointsSet.plotStyle = style
            p.addPlot(pointsSet)
            // plotting interpolation function points
            style = PlotStyle(Style.POINTS)
            style.setPointType(1)
            style.setPointSize(1)
            if (interpolationSet != null) interpolationSet.plotStyle = style
            p.addPlot(interpolationSet)
            p.plot()
        }
    }
}
