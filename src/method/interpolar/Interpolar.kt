package method.interpolar

import FileFun.fileReaderInterpolar
import kotlin.math.abs

class Interpolar {
    class Vars {
        var xSearch: Double? = null
        var result: Double = 0.0
        var eUsech: Double = 0.0
        var eOkr: Double = 5 * 1E-5
        var eReal: Double = 0.0
    }

    class Number {
        var value: Double? = null
        var coefficient: Double = 1.0
        var degree: Double = 0.0
        var isKnown: Boolean = false
    }

    companion object {
        fun methodInterpolar(): Int {
            val number = Number()
            number.degree = 0.5
            number.isKnown = false

            val vars = Vars()
            val array = fileReaderInterpolar("Files/Interpolar/Interpolar.txt") ?: return -1
            print("Search y for x: ")
            vars.xSearch = readLine()!!.toDouble()
            println("P(${vars.xSearch}) = ")
            for (i in 0..(array.size - 1)) {
                var temp = 1.0
                for (j in 0..(array.size - 1))
                    if (j != i) temp *= (vars.xSearch!! - array[j][0]) / (array[i][0] - array[j][0])
                vars.result += temp * array[i][1]
                println("\tTemp -> ${"%.4f".format(temp * array[i][1])}")
            }
            println("Result -> ${"%.4f".format(vars.result)}\n\n")

            var factor = 1.0
            var temp = 1.0
            for (i in 1..(array.size)) {
                factor *= i
                temp *= (vars.xSearch!! - i)
                println("Temp -> ${vars.xSearch!!} - $i")
            }
            println("Temp result -> ${"%.4f".format(temp)}")
            println("Factorial -> $factor")
            val M4 = findM(array.size - 1, number)
            vars.eUsech = (M4/ factor) * abs(temp)
            vars.eReal = vars.eOkr + vars.eUsech
            println("M4(${array.size}) -> ${"%.4f".format(M4)}")
            println("E truncated -> ${"%.4f".format(vars.eUsech)}")
            println("E real -> ${"%.4f".format(vars.eReal)}")
            return 0
        }

        private fun findM(step: Int, x: Number): Double {
            for (i in 0..step) {
                x.coefficient *= x.degree
                x.degree -= 1.0
            }
            return abs(x.coefficient)
        }
    }
}