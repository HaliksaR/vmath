package method.differentialEquations

import kotlin.math.absoluteValue
import kotlin.reflect.KFunction2
import kotlin.reflect.KFunction4

class DiffEqWithDoubleConvers {
    companion object {
        fun methodDiffEqWithDoubleConvers(): Int {
            val h = 0.1
            val eps = 0.001
            var yEuler = 1.0
            var yKutta2 = 1.0
            var yKutta3 = 1.0
            var yKutta4 = 1.0

            var i = 0.0
            while (i < 0.5) {
                yEuler = eulerResult(i, yEuler, h, eps, ::func)
                yKutta2 = kuttaResult(i, yKutta2, h, eps, ::func, 2)
                yKutta3 = kuttaResult(i, yKutta3, h, eps, ::func, 3)
                yKutta4 = kuttaResult(i, yKutta4, h, eps, ::func, 4)
                println("x = ${i + h}\n" +
                        " : Euler: $yEuler\n" +
                        " : Kutta2: $yKutta2\n" +
                        " : Kutta3: $yKutta3\n" +
                        " : Kutta4: $yKutta4\n")
                i += h
            }
            return 0
        }

        private fun kuttaResult(
            x: Double,
            y: Double,
            h: Double,
            eps: Double,
            kFunction: KFunction2<Double, Double, Double>,
            num: Int
        ): Double {
            return when (num) {
                2 -> diffur(x, y, h, eps, kFunction, ::kutta2)
                3 -> diffur(x, y, h, eps, kFunction, ::kutta3)
                4 -> diffur(x, y, h, eps, kFunction, ::kutta4)
                else -> 0.0
            }
        }

        private fun kutta2(
            x: Double,
            y: Double,
            h: Double,
            kFunction: KFunction2<
                    @ParameterName(name = "x") Double,
                    @ParameterName(name = "y") Double,
                    Double>
        ): Double = y + (h / 2) * (kFunction(x, y) + (kFunction(x + h, euler(x, y, h, kFunction))))

        private fun kutta3(
            x: Double,
            y: Double,
            h: Double,
            kFunction: KFunction2<
                    @ParameterName(name = "x") Double,
                    @ParameterName(name = "y") Double,
                    Double>
        ): Double = y + h * kFunction(x + h / 2, euler(x, y, h / 2, kFunction))

        private fun kutta4(
            x: Double,
            y: Double,
            h: Double,
            kFunction: KFunction2<
                    @ParameterName(name = "x") Double,
                    @ParameterName(name = "y") Double,
                    Double>
        ): Double {
            val func1Result = kFunction(x, y)
            val func2Result = kFunction(x + h / 2.0, y + h / 2.0 * func1Result)
            val func3Result = kFunction(x + h / 2.0, y + h / 2.0 * func2Result)
            val func4Result = kFunction(x + h, y + h * func3Result)
            return y + h / 6.0 * (func1Result + 2.0 * func2Result + 2.0 * func3Result + func4Result)
        }

        private fun eulerResult(
            x: Double,
            y: Double,
            h: Double,
            eps: Double,
            kFunction: KFunction2<
                    @ParameterName(name = "x") Double,
                    @ParameterName(name = "y") Double,
                    Double>
        ): Double = diffur(x, y, h, eps, kFunction, ::euler)

        private fun diffur(
            x: Double,
            y: Double,
            h: Double,
            eps: Double,
            kFunction: KFunction2<
                    @ParameterName(name = "x") Double,
                    @ParameterName(name = "y") Double,
                    Double>,
            kMethod: KFunction4<
                    @ParameterName(name = "x") Double,
                    @ParameterName(name = "y") Double,
                    @ParameterName(name = "h") Double,
                    @ParameterName(name = "kFunction") KFunction2<
                            @ParameterName(name = "x") Double,
                            @ParameterName(name = "y") Double,
                            Double>,
                    Double>
        ): Double {
            var yH: Double
            val h2 = h / 2
            var funcH = kMethod(x, y, h, kFunction)
            do {
                yH = funcH
                funcH = y
                var j = x
                while (j < (x + h)) {
                    funcH = kMethod(j, funcH, h2, kFunction)
                    funcH /= 2
                    j += h2
                }
            } while ((yH - funcH).absoluteValue > eps)
            return funcH
        }

        private fun euler(
            x: Double,
            y: Double,
            h: Double,
            kFunction: KFunction2<
                    @ParameterName(name = "x") Double,
                    @ParameterName(name = "y") Double,
                    Double>
        ): Double = y + h * kFunction(x, y)


        private fun func(x: Double, y: Double): Double = x + y
    }
}