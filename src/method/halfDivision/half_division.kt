package method.halfDivision

import kotlin.math.pow
import kotlin.math.abs

private var EPS = 0.00001

class HalfDivision {
    class Points(Left: Double, Right: Double) {
        var left: Double = Left
        var right: Double = Right
        var middle: Double = Double.NaN
    }

    data class InitalValues(
        var powX: Int,
        var variable: Double,
        var pointOne: Double,
        var pointTWo: Double
    )

    companion object {
        fun methodHalfDivision(): Int {
            val values = InitalValues(2, 3.0, 1.0, 2.0)
            var points = Points(values.pointOne, values.pointTWo)
            println("Print condition: ")
            points.middle = middleFunc(points)
            var steps = 1
            while (abs(points.right - points.left) > EPS) {
                println("Steps: $steps, Middle: ${"%.1f".format(points.middle)}")
                middleOfInterval(points, values)
                println("[${"%.1f".format(points.left)};${"%.1f".format(points.right)}]")
                points.middle = middleFunc(points)
                steps++
            }
            println("Answer: ${"%.1f\n".format(points.middle)}")
            return 0
        }

        private fun powExtremePoints(num: Double, data: InitalValues) = num.pow(data.powX) - data.variable

        private fun middleFunc(points: Points) = (points.left + points.right) / 2

        private fun middleOfInterval(points: Points, data: InitalValues) {
            if (powExtremePoints(
                    points.left,
                    data
                ) * powExtremePoints(points.middle, data) < 0)
                points.right = points.middle
            else if (powExtremePoints(
                    points.right,
                    data
                ) * powExtremePoints(points.middle, data) < 0)
                points.left = points.middle
        }
    }
}
