package method.integration

import kotlin.math.absoluteValue
import kotlin.math.cos
import kotlin.math.sin
import kotlin.reflect.KFunction3

class NumericalIntegration {
    companion object {
        fun methodNumericalIntegration(): Int {
            val a = 0.0
            val b = 5.0
            val exp = 0.001
            println(simpsonResult(a, b, exp))
            println(trapeziumResult(a, b, exp))
            println(integralFunc(b) - integralFunc(a))
            return 0
        }

        private fun integralFunc(x: Double): Double = sin(x)
        private fun func(d: Double): Double = cos(d)

        private fun trapeziumResult(
            a: Double,
            b: Double,
            exp: Double
        ): Double = integral(a, b, exp, ::trapezium)

        private fun simpsonResult(
            a: Double,
            b: Double,
            exp: Double
        ): Double = integral(a, b, exp, ::simpson)

        private fun integral(
            a: Double,
            b: Double,
            exp: Double,
            kFunction: KFunction3<
                    @ParameterName(name = "a") Double,
                    @ParameterName(name = "b") Double,
                    @ParameterName(name = "h") Double,
                    Double>
        ): Double {
            var h = 1.0
            var funcH = kFunction(a, b, h)
            var funcH2 = kFunction(a, b, h / 2)
            h /= 2
            while ((funcH - funcH2).absoluteValue > exp) {
                funcH = funcH2
                h /= 2
                funcH2 = kFunction(a, b, h)
            }
            return funcH2
        }

        private fun trapezium(
            a: Double,
            b: Double,
            h: Double
        ): Double {
            var sum = 0.0
            var x = a
            while (x < b) {
                sum += (func(x) + func(x + h)) * h / 2
                x += h
            }
            return sum
        }

        private fun simpson(
            a: Double,
            b: Double,
            h: Double
        ): Double {
            var even = 0.0
            var odd = 0.0
            var x = a + h
            while (x < b) {
                even += func(x + h)
                odd += func(x)
                x += 2 * h
            }
            return (h / 3.0) * (func(a) + 2.0 * even + 4.0 * odd + func(b))
        }
    }
}