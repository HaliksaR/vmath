package method.sumpleIter

import FileFun.fileReader
import PrintResurses.*
import kotlin.math.abs
import kotlin.math.ln

private var EPS = 1E-4

fun MethoodSimpleIter(): Int {
    val matrix: Array<DoubleArray> = fileReader("Files/MethodSimpleIter/Matrix.txt")
    val row: Int = matrix.size - 1

    println("Input Matrix:")
    printMatrix(matrix)

    val b = DoubleArray(row)
    val c = Array(row) { DoubleArray(row) }
    val cx = DoubleArray(row)
    val result = DoubleArray(row)
    var normB = 0.0
    var normC = 0.0
    var temp: Double

    for (i in 0..(row - 1)) {
        temp = matrix[i][i]
        for (j in 0..row) {
            matrix[i][j] /= temp
        }
    }

    for (i in 0..(row - 1)) {
        for (j in 0..(row - 1))
            c[i][j] = matrix[i][j]
        c[i][i] = 0.0
        b[i] = matrix[i][row]
    }

    println("Input Vector C:")
    printVector(c)
    println("Input Vector B:")
    for (i in 0..(b.size - 1))
        print("${"%.1f\n".format(b[i])}")

    for (i in 0..(row - 1)) {
        for (j in 0..(row - 1)) {
            result[i] += abs(c[j][i])
            normC = result[0]
            if (result[i] > normC)
                normC = result[i]
        }
    }

    var tempB = 0.0
    for (i in 0..(row - 1)) {
        tempB = abs(b[i])
        if(tempB > normB)
            normB = tempB
    }

    print(
        "\nNorm C = ${"%.1f".format(normC)}" +
                "\nNorm B = ${"%.1f".format(normB)}"
    )

    val step = (ln((1 - normC) * EPS / normB) / ln(normC)) + 1
    println("\nSteps = ${"%.1f".format(step)}")

    for (i in 0..((step - 1).toInt())) {
        for (j in 0..(row - 1))
            cx[j] = 0.0
        for (j in 0..(row - 1))
            for (k in 0..(row - 1))
                cx[j] += c[j][k] * result[k]
        for (j in 0..(row - 1))
            result[j] = b[j] - cx[j]
    }

    println("\nresult:")
    for (i in 0..(row - 1))
        println("x[${i + 1}] = ${"%.3f".format(result[i])}")
    return 0
}