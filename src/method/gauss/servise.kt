package method.gauss

fun result(matrix: Array<DoubleArray>, column:Int, row:Int) {
    var size = column
    var result = DoubleArray(size)
    result[0] = matrix[row][column] / matrix[row][column - 1]
    var temp: Double
    var indexOne = 1
    for (m in (row - 1) downTo 0) {
        var indexTwo = 0
        temp = -matrix[m][column]

        for (p in (column - 1) downTo m) {
            if (p == m) {
                temp /= matrix[m][p]
                result[indexOne++] = -temp
            } else
                temp += matrix[m][p] * result[indexTwo++]
        }
    }
    println("result: ")
    for (p in (size - 1) downTo 0)
        print("${"%.1f".format(result[p])} ")
    println()
    println()
}