package method.gauss

import FileFun.*
import PrintResurses.printMatrix

fun serchMax(array:Array<DoubleArray>, index:Int):Int {
    var count = 0
    var max = Math.abs(array[index][index])
    for (i in (index + 1)..(array.size - 1)) {
        if (Math.abs(array[i][index]) > max) {
            max = array[i][index]
            count = i
        } else {
            count = index
        }
    }
    return count
}

fun swapRows(array: Array<DoubleArray>, rowOne:Int, rowTwo:Int): Array<DoubleArray> {
    var col = 0
    for (row in 0 until array.size)
        col = array[row].size - 1
    for (j in 0..col) {
        var temp = array[rowOne][j]
        array[rowOne][j] = array[rowTwo][j]
        array[rowTwo][j] = temp
    }
    return array
}

fun GaussModTask():Int {
    var matrix: Array<DoubleArray> = fileReader("Files/Gauss/Matrix.txt")
    var row: Int = matrix.size - 1
    var column = 0
    for (rows in 0 until matrix.size)
        for (col in 0 until matrix[rows].size)
            column = matrix[rows].size - 1

    println("Input Matrix:")
    printMatrix(matrix)

    var diag = 0
    for (j in 0..(row - 1)) {
        var seerchMax = serchMax(matrix, j)
        println("Changing ${j + 1} and ${seerchMax + 1} rows\n")
        if (seerchMax != j) {
            println("Changing ${j + 1} and ${seerchMax + 1} rows\n")
            matrix = swapRows(matrix, seerchMax, j)
        }
        for (k in (diag + 1)..row) {
            var ratio = -(matrix[k][diag] / matrix[diag][diag])
            for (l in j..column) matrix[k][l] += matrix[diag][l] * ratio
            for (k in 0..row) {
                for (l in 0..column) {
                    if (l == column ) print(" | ")
                    print("${"%.1f".format(matrix[k][l])} ")
                }
                println()
            }
            println()
        }
        diag++
    }
    result(matrix, column, row)
    return 0
}