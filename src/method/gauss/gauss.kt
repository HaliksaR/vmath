package method.gauss

import FileFun.*
import PrintResurses.printMatrix

fun GaussTask(): Int {
    var matrix: Array<DoubleArray> = fileReader("Files/Gauss/Matrix.txt")
    var row: Int = matrix.size - 1
    var column = 0
    for (rows in 0 until matrix.size)
        for (col in 0 until matrix[rows].size)
            column = matrix[rows].size - 1

    println("Input Matrix:")
    printMatrix(matrix)

    for (diag in 0..(row - 1)) {
        for (k in (diag + 1)..row) {
            var ratio = -(matrix[k][diag] / matrix[diag][diag])
            for (l in 0..column) matrix[k][l] += matrix[diag][l] * ratio
            for (k in 0..row) {
                for (l in 0..column) {
                    if (l == column) print(" | ")
                    print("${"%.1f".format(matrix[k][l])} ")
                }
                println()
            }
            println()
        }
    }
    result(matrix, column, row)
    return 0
}