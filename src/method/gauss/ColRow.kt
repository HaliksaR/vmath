package method.gauss

class ColRow {
    var col: Int = 0
        set(value) {
            if (value >= 0) field = value
        }
    var row: Int = 0
        set(value) {
            if (value >= 0) field = value
        }
}