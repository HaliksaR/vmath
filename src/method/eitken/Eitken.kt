package method.eitken

import FileFun.fileReaderInterpolar

class Eitken {
    companion object {
        fun methodEitken(): Int {
            print("Search y for x: ")
            val xSearch = readLine()!!.toDouble()
            val array = fileReaderInterpolar("Files/Interpolar/Interpolar.txt") ?: return -1
            for (i in 0 until array.size)
                for (j in (i - 1) downTo 0) {
                    println("y(${j+1}) = ${array[j + 1][1]}")
                    println("y($j) = ${array[j][1]}")
                    println("x($i) = ${array[i][0]}")
                    println("x($j) = ${array[j][0]}")
                    array[j][1] =
                        array[j + 1][1] + (array[j + 1][1] - array[j][1]) * (
                                xSearch - array[i][0]) / (array[i][0] - array[j][0])
                    println(array[j][1])
                }
            println(array[0][1])
            return 0
        }
    }
}
/*
p01
p12
p012
p23
p123
p0123
 */