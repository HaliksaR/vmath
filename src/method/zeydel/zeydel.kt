package method.zeydel

import FileFun.fileReader
import PrintResurses.*
import kotlin.math.ln


private var EPS = 1E-4


fun Zeydel(): Int {
    val matrix: Array<DoubleArray> = fileReader("Files/Zeydel/Matrix.txt")
    val row: Int = matrix.size - 1

    println("Input Matrix:")
    printMatrix(matrix)

    var temp: Double
    var maxB = 0.0
    var maxC = 0.0
    val b = DoubleArray(row)
    val c = Array(row) { DoubleArray(row) }
    val max = DoubleArray(row)
    val result = DoubleArray(row)

    for (i in 0..(row - 1)) {
        temp = matrix[i][i]
        for (j in 0..row) {
            matrix[i][j] /= temp
            if (j == row)
                b[i] = matrix[i][j]
            result[i] = 0.0
        }
    }

    for (i in 0..(row - 1)) {
        max[i] = 0.0
        for (j in 0..(row - 1)) {
            c[i][j] = matrix[i][j]
            c[i][i] = 0.0
            max[i] += c[i][j]
        }
    }
    for (i in 0..(row - 1)) {
        if (maxC < max[i])
            maxC = max[i]
        if (maxB < b[i])
            maxB = b[i]
    }

    var step = 0.0
    val number = ln(EPS * (1 - maxC) / maxB) / ln(maxC)

    while (step <= number) {
        for (i in 0..(row - 1)) {
            result[i] = 0.0
            for (j in 0..(row - 1))
                result[i] = result[i] + c[i][j] * result[j]
            result[i] = b[i] - result[i]
        }
        step++
    }

    println("Norm C = ${"%.1f".format(maxC)}\n" +
            "Norm B = ${"%.1f".format(maxB)}\n" +
            "Steps = ${number.toInt()}")
    for (i in 0..(row - 1))
        println("x[${i + 1}] = ${"%.1f".format(result[i])}")
    return 0
}