package method.newton

import kotlin.math.pow
import kotlin.math.abs

private var EPS = 0.00001

class Newton {
    class Points(Left: Double) {
        var left: Double = Left
        var middle: Double = Double.NaN
    }

    data class InitalValues(
        var powX: Int,
        var variable: Double,
        var pointOne: Double,
        var pointTWo: Double
    )

    companion object {
        fun methodNewton(): Int {
            val values = InitalValues(2, 3.0, 1.0, 2.0)
            var points = Points(values.pointOne)

            println("Print condition: ")
            points.middle = middleFunc(points.left, values)
            var ysl = points.middle
            var steps = 1
            var temp: Double
            while (abs(ysl) > EPS) {
                temp = points.middle
                println("Steps: $steps, Middle: ${"%.1f".format(points.middle)}")
                points.middle = middleFunc(points.middle, values)
                ysl = points.middle - temp
                steps++
            }
            println("Answer: ${"%.1f\n".format(points.middle)}")
            return 0
        }

        private fun powExtremePoints(num: Double, data: InitalValues) = num.pow(data.powX) - data.variable
        private fun powExtremePoints(num: Double) = 2 * num
        private fun middleFunc(number: Double, data: InitalValues) =
            -(powExtremePoints(number, data) / powExtremePoints(number)) + number
    }
}
