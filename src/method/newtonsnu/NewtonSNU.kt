package method.newtonsnu

import kotlin.math.pow
import kotlin.math.sqrt


class NewtonSNU {


    companion object {
        private lateinit var arrayA: Array<Array<Double>>
        private lateinit var arrayB: Array<Double>
        private var dx: Double = 0.0
        private var dy: Double = 0.0
        private val eps = 0.001

        fun methodNewtonSNU(): Int {

            print("x = ")
            var x: Double = readLine()!!.toDouble()
            print("y = ")
            var y: Double = readLine()!!.toDouble()

            var norm: Double
            arrayA = Array<Array<Double>>(2) { Array<Double>(2) { 0.0 } }
            arrayB = Array<Double>(2) { 0.0 }

            do {
                for (i in 0 until arrayA.size)
                    for (j in 0 until arrayA.size)
                        arrayA[i][j] = action(j + 1 + 10 * (i + 1)).invoke(x, y)

                revert(arrayA)

                dx = -arrayA[0][0] * action(1).invoke(x, y) + -arrayA[1][0] * action(2).invoke(x, y)
                dy = -arrayA[0][1] * action(1).invoke(x, y) + -arrayA[1][1] * action(2).invoke(x, y)

                x += dx
                y += dy

                for (i in 0..1)
                    arrayB[i] = action(i + 1).invoke(x, y)

                norm = sqrt(arrayB[0].pow(2) + arrayB[1].pow(2))
            } while (norm >= eps)

            println("%.4f".format(x))
            println("%.4f".format(y))
            return 0
        }

        private fun revert(arrayA: Array<Array<Double>>) {
            val det: Double = arrayA[0][0] * arrayA[1][1] - arrayA[0][1] * arrayA[1][0]
            var temp: Double = arrayA[0][0]
            arrayA[0][0] = arrayA[1][1] / det
            arrayA[1][1] = temp / det
            temp = arrayA[0][1]
            arrayA[0][1] = -arrayA[1][0] / det
            arrayA[1][0] = -temp / det
        }

        private fun action(number: Int): (Double, Double) -> Double {
            when (number) {
                1 -> return { x: Double, y: Double -> x * x + y * y * y - 4 }
                2 -> return { x: Double, y: Double -> x / y - 2 }
                11 -> return { x: Double, _: Double -> 2 * x }
                12 -> return { _: Double, y: Double -> 3 * y * y }
                21 -> return { _: Double, y: Double -> 1 / y }
                22 -> return { x: Double, y: Double -> -x / (y * y) }
            }
            return { _: Double, _: Double -> -1.0 }
        }
    }
}
