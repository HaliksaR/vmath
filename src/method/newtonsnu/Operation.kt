package method.newtonsnu

interface Operation {
    fun execute(x: Double, y: Double): Double
}