package method.approximation

import FileFun.fileReaderInterpolar
import com.panayotis.gnuplot.JavaPlot
import com.panayotis.gnuplot.plot.DataSetPlot
import com.panayotis.gnuplot.style.PlotStyle
import com.panayotis.gnuplot.style.Style
import kotlin.collections.ArrayList


class Approximation {
    abstract class BaseApproximation(xArray: DoubleArray, yArray: DoubleArray) {
        protected var arrayX: DoubleArray = xArray.clone()
        protected var arrayY: DoubleArray = yArray.clone()
        var min: Double = 0.toDouble()
            protected set
        var max: Double = 0.toDouble()
            protected set
        val argumentsRange: Double

        abstract fun approximazeFunction(point: Double): Double

        init {
            checkDublicateArguments()
            var max = java.lang.Double.MIN_VALUE
            var min = java.lang.Double.MAX_VALUE
            for (i in arrayX.indices) {
                if (arrayX[i] > max) {
                    max = arrayX[i]
                }
                if (arrayX[i] < min) {
                    min = arrayX[i]
                }
            }
            this.min = min
            this.max = max
            this.argumentsRange = max - min
        }

        protected fun checkInterval(point: Double) {
            if (point < min || point > max) {
                println(
                    String.format(
                        "Point %s outside the predetermined interval. Extrapolation is not possible",
                        point
                    )
                )
                System.exit(1)
            }
        }

        private fun checkDublicateArguments() {
            for (i in arrayX.indices) {
                for (j in i + 1 until arrayX.size) {
                    if (arrayX[i] == arrayX[j]) {
                        println("Array of arguments containts dublicate values. Interpolation is not possible")
                        System.exit(1)
                    }
                }
            }
        }
    }

    class Lagrange(xArray: DoubleArray, yArray: DoubleArray) : BaseApproximation(xArray, yArray) {
        override fun approximazeFunction(point: Double): Double {
            checkInterval(point)
            var valueInPoint = 0.0
            for (i in arrayX.indices) {
                var polinom = 1.0
                for (j in arrayY.indices)
                    if (i != j) polinom *= (point - arrayX[j]) / (arrayX[i] - arrayX[j])
                valueInPoint += arrayY[i] * polinom
            }
            return valueInPoint
        }
    }

    companion object {
        private lateinit var points: Array<DoubleArray>
        fun methodApproximation(): Int {
            points = fileReaderInterpolar("Files/Approximation/Approximation.txt")?: return  -1
            val y = ArrayList<Double>()
            val x = ArrayList<Double>()
            for (i in points.indices) {
                x.add(points[i][0])
                y.add(points[i][1])
            }
            val lagrange = Lagrange(x.toDoubleArray(), y.toDoubleArray())
            val point = readLine()!!.toDouble()
            visualize(lagrange)
            println("Input approximation point: ")
            println("%.3f".format(lagrange.approximazeFunction(point)))
            return 0
        }

        private fun visualize(lagrange: BaseApproximation) {
            val pointsMod = ArrayList<Double>()
            val x = ArrayList<Double>()
            run {
                var i = lagrange.min
                while (i < lagrange.max) {
                    pointsMod.add(lagrange.approximazeFunction(i))
                    x.add(i)
                    i += 0.1
                }
            }
            pointsMod.add(lagrange.approximazeFunction(lagrange.max))
            x.add(lagrange.max)
            val setPoints = Array(x.size) { DoubleArray(2) }
            for (j in setPoints.indices) {
                setPoints[j][0] = x[j]
                setPoints[j][1] = pointsMod[j]
            }

            for (i in pointsMod.indices)
                println(pointsMod[i])

            val p = JavaPlot()
            p.setTitle("Approximation")
            // measurement points
            val pointsSet = DataSetPlot(setPoints)
            // Approximation function
            val ApproximationSet = DataSetPlot(points)
            // plotting measurement points
            var style = PlotStyle(Style.POINTS)
            style.setPointType(7)
            style.setPointSize(2)
            pointsSet.plotStyle = style
            p.addPlot(pointsSet)
            // plotting Approximation function points
            style = PlotStyle(Style.POINTS)
            style.setPointType(1)
            style.setPointSize(1)
            ApproximationSet.plotStyle = style
            p.addPlot(ApproximationSet)
            p.plot()
        }
    }
}