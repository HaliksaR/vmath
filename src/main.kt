import method.approximation.Approximation
import method.differentialEquations.DiffEqWithDoubleConvers
import method.gauss.GaussModTask
import method.gauss.GaussTask
import method.sumpleIter.MethoodSimpleIter
import method.zeydel.Zeydel
import method.halfDivision.HalfDivision
import method.hord.Hord
import method.newton.Newton
import method.interpolar.Interpolar
import method.eitken.Eitken
import method.integration.NumericalIntegration
import method.newtonsnu.NewtonSNU
import method.interpolar.newton.InterpolarNewton
import method.interpolar.spline.InterpolarSpline
import method.interpolar.trigonometry.InterpolarTrigonometry

fun tasks() = println(
    "1} Gauss\n" +
            "\t1.1} Gauss modified\n" +
            "2} Method Simple Iter\n" +
            "3} Method Zeydel\n" +
            "4} Method Half Division\n" +
            "5} Method Hord\n" +
            "6} Method Newton\n" +
            "7} Method Interpolar\n" +
            "8} Method Eitken\n" +
            "9} Method NewtonSNU\n" +
            "10} Method Interpolar Newton\n" +
            "11} Method Interpolar Spline\n" +
            "12} Method Interpolar Trigonometry\n" +
            "13} Method Numerical integration (with double conversion)\n" +
            "14} Solving differential equations with double conversion\n" +
            "15} Approximation\n" +
            "\nAdditional tasks" +
            ""
)

fun taskAnswer(): Int {
    println("\n\n\n\n")
    tasks()
    print("Choose task: ")
    when (readLine()!!.toDouble()) {
        0.0 -> return -1
        1.0 -> return GaussTask()
        1.1 -> return GaussModTask()
        2.0 -> return MethoodSimpleIter()
        3.0 -> return Zeydel()
        4.0 -> return HalfDivision.methodHalfDivision()
        5.0 -> return Hord.methodHord()
        6.0 -> return Newton.methodNewton()
        7.0 -> return Interpolar.methodInterpolar()
        8.0 -> return Eitken.methodEitken()
        9.0 -> return NewtonSNU.methodNewtonSNU()
        10.0 -> return InterpolarNewton.methodInterpolarNewton()
        11.0 -> return InterpolarSpline.methodInterpolarSpline()
        12.0 -> return InterpolarTrigonometry.methodInterpolarTrigonometry()
        13.0 -> return NumericalIntegration.methodNumericalIntegration()
        14.0 -> return DiffEqWithDoubleConvers.methodDiffEqWithDoubleConvers()
        15.0 -> return Approximation.methodApproximation()
    }
    return -1
}

fun main() {
    while (taskAnswer() == 0) {
    }
}
