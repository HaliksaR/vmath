package FileFun

import method.gauss.ColRow
import java.io.File
import java.io.FileNotFoundException
import kotlin.system.exitProcess

fun getColRows(): ColRow {
    val classGet = ColRow()
    var rowSave = 0
    val pathname = "Files/Gauss/Matrix.txt"
    if (File(pathname).exists()) {
        File(pathname).forEachLine {
            classGet.col = 1
            for (i in it.indices)
                if (it[i] == ' ') classGet.col++
            if (rowSave < classGet.col) rowSave = classGet.col
            classGet.row++
        }
        return classGet
    } else {
        exitProcess(-1)
    }
}

fun fileReader(pathname: String): Array<DoubleArray> {
    val classGet = getColRows()
    var maxCol = 0
    var maxRow: Int
    val matrix = Array(classGet.row) {
        DoubleArray(classGet.col)
    }
    if (File(pathname).exists()) {
        File(pathname).forEachLine {
            val lists = it.split(Regex("\\s+"))
            maxRow = 0
            for (list in lists) {
                matrix[maxCol][maxRow] = list.toDouble()
                maxRow++
            }
            for (ind in maxRow until classGet.row) {
                matrix[maxCol][ind] = 0.0
            }
            maxCol++
        }
    } else {
        exitProcess(-2)
    }
    return matrix
}


fun fileReaderInterpolar(pathname: String): Array<DoubleArray>? {
    try {
        var number = 0
        File(pathname).forEachLine {
            number++
        }
        val matrix = Array(number) {
            DoubleArray(2)
        }
        var i = 0
        File(pathname).forEachLine {
            val lists = it.split(Regex("\\s+"))
            for ((j, list) in lists.withIndex()) {
                matrix[i][j] = list.toDouble()
            }
            i++
        }
        for (j in 0 until matrix.size) {
            println("${matrix[j][0]}  ${matrix[j][1]}")
        }
        return matrix
    } catch (e: FileNotFoundException) {
        println("FILE NOT FOUND")
        return null
    }
}

fun fileReaderline(pathname: String): DoubleArray {
    var number = 0
    File(pathname).forEachLine {
        number++
    }
    val line = DoubleArray(number)
    if (File(pathname).exists()) {
        var i = 0
        File(pathname).forEachLine {
            line[i] = it.toDouble()
            i++
        }
    } else exitProcess(-2)
    return line
}