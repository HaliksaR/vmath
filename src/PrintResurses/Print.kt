package PrintResurses

fun printMatrix(array: Array<DoubleArray>) {
    for (row in array.indices) {
        for (column in 0 until array[row].size)
            print("${array[row][column]} ")
        println()
    }
    println()
}

fun printVector(array: Array<DoubleArray>) {
    for (row in 0 until array.size) {
        for (column in 0 until array[row].size)
            print("${"%.1f".format(array[row][column])} ")
        println()
    }
}

